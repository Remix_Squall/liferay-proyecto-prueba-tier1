DROP TABLE IF EXISTS lrm_project_tareas;
DROP TABLE IF EXISTS lrm_project;
DROP TABLE IF EXISTS lrm_tareas;

CREATE TABLE lrm_project(
	codigo VARCHAR(6) NOT NULL,
	descripcion TEXT,
	fecha_inicio DATE,
	coste FLOAT,
	PRIMARY KEY (codigo)
)ENGINE=innoDB DEFAULT CHARSET=utf8;

CREATE TABLE lrm_tareas(
	tarea VARCHAR(30) NOT NULL,
	PRIMARY KEY (tarea)
)ENGINE=innoDB DEFAULT CHARSET=utf8;

CREATE TABLE lrm_project_tareas(
	cod_proj VARCHAR(6) NOT NULL,
	tarea VARCHAR(30) NOT NULL,
	PRIMARY KEY (cod_proj,tarea),
	FOREIGN KEY (cod_proj) REFERENCES lrm_project(codigo),
	FOREIGN KEY (tarea) REFERENCES lrm_tareas(tarea)
)ENGINE=innoDB DEFAULT CHARSET=utf8;

INSERT INTO lrm_project VALUES(
	'PR0001','Ejemplo de descripción del primer proyecto',
	'2013-11-24',3500.00
);
INSERT INTO lrm_project VALUES(
	'PR0002','Ejemplo de descripción del segundo proyecto',
	'2014-08-14',6000.00
);
INSERT INTO lrm_project VALUES(
	'PR0003','Ejemplo de descripción del tercer proyecto',
	'2010-12-01',7999.99
);
INSERT INTO lrm_project VALUES(
	'PR0004','Ejemplo de descripción del cuarto proyecto',
	'2014-12-17',500.01);

INSERT INTO lrm_tareas VALUES('desarrollo');
INSERT INTO lrm_tareas VALUES('diseño');
INSERT INTO lrm_tareas VALUES('maquetación');
INSERT INTO lrm_tareas VALUES('mantenimiento');
INSERT INTO lrm_tareas VALUES('depuración');
INSERT INTO lrm_tareas VALUES('testing');

INSERT INTO lrm_project_tareas VALUES('PR0001','desarrollo');
INSERT INTO lrm_project_tareas VALUES('PR0001','diseño');
INSERT INTO lrm_project_tareas VALUES('PR0001','depuración');
INSERT INTO lrm_project_tareas VALUES('PR0002','desarrollo');
INSERT INTO lrm_project_tareas VALUES('PR0002','testing');
INSERT INTO lrm_project_tareas VALUES('PR0002','mantenimiento');
INSERT INTO lrm_project_tareas VALUES('PR0002','maquetacion');
INSERT INTO lrm_project_tareas VALUES('PR0003','desarrollo');
INSERT INTO lrm_project_tareas VALUES('PR0003','maquetacion');
INSERT INTO lrm_project_tareas VALUES('PR0003','diseño');
INSERT INTO lrm_project_tareas VALUES('PR0004','mantenimiento');