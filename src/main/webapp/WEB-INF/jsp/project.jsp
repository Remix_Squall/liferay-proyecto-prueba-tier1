<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<style type="text/css">
.data, .data td {
	border-collapse: collapse;
	width: 100%;
	border: 1px solid #aaa;
	margin: 2px;
	padding: 2px;
}

.data th {
	font-weight: bold;
	background-color: #5C82FF;
	color: white;
}
</style>
<h2>Contact Manager</h2>
<portlet:actionURL var="addProjectURL">
	<portlet:param name="action" value="add"></portlet:param>
</portlet:actionURL>
<portlet:actionURL var="deleteProjectURL">
	<portlet:param name="action" value="delete"></portlet:param>
</portlet:actionURL>
<form:form name="project" modelAttribute="project" method="post"
	action="${addProjectURL}">
	<table>
		<tr>
			<td><form:label path="codigo">
					<spring:message code="label.descripcion" />
				</form:label></td>
			<td><form:input path="codigo" /></td>
		</tr>
		<tr>
			<td><form:label path="descripcion">
					<spring:message code="label.descripcion" />
				</form:label></td>
			<td><form:input path="descripcion" /></td>
		</tr>
		<tr>
			<td><form:label path="coste">
					<spring:message code="label.coste" />
				</form:label></td>
			<td><form:input path="coste" /></td>
		</tr>
		<tr>
			<td><form:label path="fechaInicio">
					<spring:message code="label.fechaInicio" />
				</form:label></td>
			<td><form:input path="fechaInicio" /></td>
		</tr>
		<tr>
			<td><form:label path="telephone">
					<spring:message code="label.fechaInicio" />
				</form:label></td>
			<td><form:input path="telephone" /></td>
		</tr>
		<tr>
			<td colspan="2"><input type="submit"
				value="<spring:message code="label.addproject"/>" /></td>
		</tr>
	</table>
</form:form>


<h3>Contacts</h3>
<c:if test="${!empty projectList}">
	<table class="data">
		<tr>
			<th>C�digo</th>
			<th>Descripci�n de proyecto</th>
			<th>Fecha de inicio</th>
			<th>Coste</th>
			<th>#</th>
		</tr>
		<c:forEach items="${contactList}" var="contact">
			<tr>
				<td>${project.codigo}</td>
				<td>${contact.descripcion}</td>
				<td>${contact.fecha_inicio}</td>
				<td>${contact.coste}</td>
				<td><a href="${deleteProjectURL}&contactId=${project.codigo}">delete</a></td>
			</tr>
		</c:forEach>
	</table>
</c:if>


</body>
</html>
