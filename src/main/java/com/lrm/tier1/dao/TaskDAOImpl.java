package com.lrm.tier1.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.lrm.tier1.form.Task;

@Repository
public class TaskDAOImpl implements TaskDAO {
	
	@Autowired
	private SessionFactory sessionFactory;

	public void addTask(Task task) {
		sessionFactory.getCurrentSession().save(task);
	}

	public List<Task> getTasks() {
		return sessionFactory.getCurrentSession().
				createQuery("from lrm_tareas").list();
	}

	public void removeTask(String tarea) {
		Task task = (Task) sessionFactory.getCurrentSession().
				load(Task.class, tarea);
		if(task != null) {
			sessionFactory.getCurrentSession().delete(task);
		}
	}

}
