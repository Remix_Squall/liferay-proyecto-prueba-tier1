package com.lrm.tier1.dao;

import java.util.List;

import com.lrm.tier1.form.Project;

public interface ProjectDAO {
	
	public void addProject(Project project);
	public List<Project> getProjects();
	public void removeProject(String codigo);

}
