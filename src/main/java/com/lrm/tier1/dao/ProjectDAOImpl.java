package com.lrm.tier1.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.lrm.tier1.form.Project;

@Repository
public class ProjectDAOImpl implements ProjectDAO {
	
	@Autowired
	@Qualifier("sessionFactory")
	private SessionFactory sessionFactory;

	public void addProject(Project project) {
		sessionFactory.getCurrentSession().save(project);
	}

	public List<Project> getProjects() {
		return sessionFactory.getCurrentSession().
				createQuery("from lrm_project").list();
	}

	public void removeProject(String codigo) {
		Project project = (Project) sessionFactory.
				getCurrentSession().load(Project.class, codigo);
		if (project != null) {
			sessionFactory.getCurrentSession().delete(project);
		}
	}

}
