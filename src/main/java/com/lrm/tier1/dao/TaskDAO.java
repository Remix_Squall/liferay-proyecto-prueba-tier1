package com.lrm.tier1.dao;

import java.util.List;

import com.lrm.tier1.form.Task;

public interface TaskDAO {
	
	public void addTask(Task task);
	public List<Task> getTasks();
	public void removeTask(String tarea);

}
