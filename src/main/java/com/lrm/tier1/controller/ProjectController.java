package com.lrm.tier1.controller;

import java.io.IOException;
import java.util.Map;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.portlet.bind.annotation.ActionMapping;

import com.liferay.portal.kernel.util.ParamUtil;
import com.lrm.tier1.form.Project;
import com.lrm.tier1.service.ProjectService;

@Controller("projectController")
@RequestMapping("VIEW")
public class ProjectController {
	
	@Autowired
	private ProjectService projectService;
	
	@RequestMapping("/index")
	public String getContacts(Map<String, Object> map) {
		map.put("project", new Project());
		map.put("projectList", projectService.getProjects());
		
		return "project";
	}
	
	@ActionMapping(params = "action=add")
	public void addContact(ActionRequest actionRequest,
			ActionResponse actionResponse, Model model,
			@ModelAttribute("project") Project project,
			BindingResult result) throws IOException, PortletException {
		System.out.println("result"+ParamUtil.getString(actionRequest, 
				"codigo"));
		System.out.println("project"+project.getCodigo());
		projectService.addProject(project);
	}
	
	@ActionMapping(params = "action=delete")
	public void deleteProject(@RequestParam("prCodigo")
	String codigo, ActionRequest actionRequest, ActionResponse actionResponse,
	Model model) {
		projectService.removeProject(codigo);
	}

}
