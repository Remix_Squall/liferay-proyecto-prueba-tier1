package com.lrm.tier1.form;

import java.util.GregorianCalendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="lrm_project")
public class Project {
	
	@Id
	@Column(name="codigo")
	private String codigo;
	
	@Column(name="descripcion")
	private String desc;
	
	@Column(name="fecha_inicio")
	private GregorianCalendar fInicio;
	
	@Column(name="coste")
	private Double coste;
	
	public String getCodigo() {
		return codigo;
	}
	
	public String getDescripcion() {
		return desc;
	}
	
	public GregorianCalendar getFInicio() {
		return fInicio;
	}
	
	public Double getCoste() {
		return coste;
	}
	
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	
	public void setDescripcion(String desc) {
		this.desc = desc;
	}
	
	public void setFInicio(GregorianCalendar fInicio) {
		this.fInicio = fInicio;
	}
	
	public void setCoste(Double coste) {
		this.coste = coste;
	}

}
