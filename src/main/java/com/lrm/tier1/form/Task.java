package com.lrm.tier1.form;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tareas")
public class Task {
	
	@Id
	@Column(name="tarea")
	private String tarea;
	
	public String getTarea() {
		return tarea;
	}
	
	public void setTarea(String tarea) {
		this.tarea = tarea;
	}

}
