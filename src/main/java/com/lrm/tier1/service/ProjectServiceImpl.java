package com.lrm.tier1.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.lrm.tier1.dao.ProjectDAO;
import com.lrm.tier1.form.Project;

@Service
public class ProjectServiceImpl implements ProjectService {
	
	@Autowired
	private ProjectDAO projectDAO;

	@Transactional
	public void addProject(Project project) {
		projectDAO.addProject(project);
	}

	@Transactional
	public List<Project> getProjects() {
		return projectDAO.getProjects();
	}

	@Transactional
	public void removeProject(String codigo) {
		projectDAO.removeProject(codigo);
	}

}
