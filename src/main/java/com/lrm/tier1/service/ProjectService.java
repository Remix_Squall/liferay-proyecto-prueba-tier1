package com.lrm.tier1.service;

import java.util.List;

import com.lrm.tier1.form.Project;

public interface ProjectService {
	
	public void addProject(Project project);
	public List<Project> getProjects();
	public void removeProject(String codigo);

}
