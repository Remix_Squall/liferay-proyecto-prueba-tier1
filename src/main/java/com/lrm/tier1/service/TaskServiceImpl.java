package com.lrm.tier1.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.lrm.tier1.dao.TaskDAO;
import com.lrm.tier1.form.Task;

@Service
public class TaskServiceImpl implements TaskService {
	
	@Autowired
	private TaskDAO taskDAO;

	@Transactional
	public void addTask(Task task) {
		taskDAO.addTask(task);
	}

	@Transactional
	public List<Task> getTasks() {
		return taskDAO.getTasks();
	}

	@Transactional
	public void removeTask(String tarea) {
		taskDAO.removeTask(tarea);
	}

}
