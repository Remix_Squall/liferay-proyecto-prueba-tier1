package com.lrm.tier1.service;

import java.util.List;

import com.lrm.tier1.form.Task;

public interface TaskService {
	
	public void addTask(Task task);
	public List<Task> getTasks();
	public void removeTask(String tarea);

}
